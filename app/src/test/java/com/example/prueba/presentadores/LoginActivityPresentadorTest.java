package com.example.prueba.presentadores;

import android.app.Activity;
import android.test.InstrumentationTestCase;

import com.example.prueba.LoginActivity;

import org.junit.Assert;
import org.junit.Test;
import org.robolectric.Robolectric;

public class LoginActivityPresentadorTest  {

    @Test
    public void testIniciarSesion() throws Exception {
        Activity activity = Robolectric.setupActivity(LoginActivity.class);

        LoginActivityPresentador loginActivityPresentador = new LoginActivityPresentador(null, (LoginActivity) activity, activity.getBaseContext());
        Assert.assertEquals(true, loginActivityPresentador.iniciarSesion("Francisco", "1234"));

    }


}