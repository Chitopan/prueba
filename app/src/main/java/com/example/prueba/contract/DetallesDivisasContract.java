package com.example.prueba.contract;

import android.widget.MultiAutoCompleteTextView;

import com.example.prueba.DetallesDivisasActivity;

public interface DetallesDivisasContract {
    interface View {
        void onSuccess(String mensaje);

        void onError(String mensaje);
    }

    interface Presenter {
      void  obtenerDatosSerializables(MultiAutoCompleteTextView texto);
    }
}
