package com.example.prueba.contract;

public interface LoginActivityContract {

    interface View {
        void onSuccess(String mensaje);
        void onError(String mensaje);
    }

    interface Presenter{

        boolean iniciarSesion(String usuario,String contraseña);

        void iniciarSharedPreferences();

    }
}
