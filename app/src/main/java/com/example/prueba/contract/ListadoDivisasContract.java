package com.example.prueba.contract;

import com.example.prueba.interfaces.Datos;
import com.example.prueba.modelos.Example;

import java.util.List;

public interface ListadoDivisasContract {

    interface View {
        void onSuccess(String mensaje);

        void onError(String mensaje);
    }

    interface Presenter {
        void cerrarSesion();
        void solicitarDatosDivisas();
        void desplegarListado(List<Datos> o);
        List<Datos> generarArreglo(Example e);
    }
}
