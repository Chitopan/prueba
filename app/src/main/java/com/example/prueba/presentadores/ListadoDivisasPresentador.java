package com.example.prueba.presentadores;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.recyclerview.widget.RecyclerView;

import com.example.prueba.ListadoDivisasActivity;
import com.example.prueba.LoginActivity;
import com.example.prueba.adaptadores.AdaptadorListadoDivisas;
import com.example.prueba.contract.ListadoDivisasContract;
import com.example.prueba.interfaces.Datos;
import com.example.prueba.interfaces.Valores;
import com.example.prueba.modelos.Example;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ListadoDivisasPresentador implements ListadoDivisasContract.Presenter {

    private ListadoDivisasActivity listadoDivisasActivity;
    private Example datos;
    private AdaptadorListadoDivisas mAdapter;
    private RecyclerView mRecyclerView;

    ListadoDivisasContract.View view;

    public ListadoDivisasPresentador(ListadoDivisasActivity listadoDivisasActivity, RecyclerView mRecyclerView) {
        this.view = listadoDivisasActivity;
        this.mRecyclerView = mRecyclerView;
        this.listadoDivisasActivity = listadoDivisasActivity;

    }

    public AdaptadorListadoDivisas getmAdapter() {
        return mAdapter;
    }

    @Override
    public void cerrarSesion() {
        SharedPreferences preferences = listadoDivisasActivity.getApplicationContext().getSharedPreferences("login", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = preferences.edit();
        editor.remove("isUserLogin");
        editor.remove("contraseña");
        editor.remove("nombre");
        editor.remove("usuario");
        editor.apply();

        view.onSuccess("Cerrando Sesion");

        Intent intent = new Intent(listadoDivisasActivity.getApplicationContext(), LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        listadoDivisasActivity.getApplicationContext().startActivity(intent);
        listadoDivisasActivity.finish();
    }

    @Override
    public void solicitarDatosDivisas() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://www.mindicador.cl/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Valores valores = retrofit.create(Valores.class);
        Call<Example> call = valores.getDivisa();

        call.enqueue(new Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                try {
                    if (response.isSuccessful()) {
                        Example example = response.body();
                        datos = example;
                        desplegarListado(generarArreglo(datos));
                    }
                } catch (Exception exception) {
                    view.onError("Hubo un problema al cargar los datos");
                }
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
                System.out.println(t);
            }
        });
    }

    @Override
    public void desplegarListado(List<Datos> o) {
        mAdapter = new AdaptadorListadoDivisas(o, listadoDivisasActivity.getApplicationContext());
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public List<Datos> generarArreglo(Example e) {

        List<Datos> listado = new ArrayList<Datos>();
        listado.add(e.getBitcoin());
        listado.add(e.getDolar());
        listado.add(e.getDolarIntercambio());
        listado.add(e.getEuro());
        listado.add(e.getImacec());
        listado.add(e.getIpc());
        listado.add(e.getIvp());
        listado.add(e.getLibraCobre());
        listado.add(e.getTasaDesempleo());
        listado.add(e.getTpm());
        listado.add(e.getUf());
        listado.add(e.getUtm());

        return listado;
    }
}
