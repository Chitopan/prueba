package com.example.prueba.presentadores;

import android.content.Intent;
import android.widget.MultiAutoCompleteTextView;

import com.example.prueba.DetallesDivisasActivity;
import com.example.prueba.contract.DetallesDivisasContract;

import java.io.Serializable;

public class DetallesDivisasPresentador  implements DetallesDivisasContract.Presenter {
    DetallesDivisasActivity detallesDivisasActivity;

    public DetallesDivisasPresentador(DetallesDivisasActivity detallesDivisasActivity) {
        this.detallesDivisasActivity = detallesDivisasActivity;
    }

    @Override
    public void obtenerDatosSerializables(MultiAutoCompleteTextView texto) {
        Intent intent = detallesDivisasActivity.getIntent();
        Serializable dato = intent.getSerializableExtra("Objeto");

        texto.setText(dato.toString());
    }
}
