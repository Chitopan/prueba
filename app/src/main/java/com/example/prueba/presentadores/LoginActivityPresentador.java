package com.example.prueba.presentadores;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.example.prueba.ListadoDivisasActivity;
import com.example.prueba.LoginActivity;
import com.example.prueba.contract.LoginActivityContract;

public class LoginActivityPresentador implements LoginActivityContract.Presenter {

    LoginActivityContract.View view;
    LoginActivity loginActivity;
    Context context;

    public LoginActivityPresentador(LoginActivityContract.View view, LoginActivity loginActivity, Context context) {
        this.context = context;
        this.view = view;
        this.loginActivity = loginActivity;
    }

    public LoginActivityPresentador(Context context) {
        this.context = context;
    }

    @Override
    public boolean iniciarSesion(String usuario, String contraseña) {

        boolean todoBien = true;

        if (usuario.trim().equals("")) {
            view.onError("Debe ingresar su correo");
            todoBien = false;
        }
        if (contraseña.trim().equals("")) {
            view.onError("Debe ingresar su contraseña");
            todoBien = false;
        }
        if (todoBien) {
            SharedPreferences preferences = context.getSharedPreferences("login", Context.MODE_PRIVATE);

            if (preferences.getString("contraseña", "").trim().equals(contraseña) &&
                    preferences.getString("nombre", "").trim().equals(usuario)) {

                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("isUserLogin", true);
                editor.apply();

                view.onSuccess("Se inicio sesion");
                Intent intent = new Intent(context.getApplicationContext(), ListadoDivisasActivity.class);
                loginActivity.startActivity(intent);
                loginActivity.finish();
            } else {
                view.onError("Verifique credenciales");
            }
        }
        return todoBien;
    }


    @Override
    public void iniciarSharedPreferences() {
        SharedPreferences preferences = context.getSharedPreferences("login", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        System.out.println("isUserLogin " + preferences.contains("isUserLogin"));
        System.out.println("contraseña " + preferences.contains("contraseña"));
        System.out.println("contraseña obtenida :" + preferences.getString("contraseña", "No existe registro"));
        System.out.println("usuario obtenido :" + preferences.getString("user", "No existe registro"));

        if (!preferences.contains("isUserLogin")) {

            editor.putString("contraseña", "1234");
            editor.putString("nombre", "Francisco");
            editor.putString("usuario", "user");
            editor.putBoolean("isUserLogin", false);
            editor.apply();

        } else {
            Toast.makeText(context.getApplicationContext(), "Listo para iniciar sesion", Toast.LENGTH_LONG).show();
        }
    }

}
