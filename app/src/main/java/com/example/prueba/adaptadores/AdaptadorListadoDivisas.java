package com.example.prueba.adaptadores;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.prueba.DetallesDivisasActivity;
import com.example.prueba.R;
import com.example.prueba.interfaces.Datos;


import java.io.Serializable;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

public class AdaptadorListadoDivisas extends RecyclerView.Adapter<AdaptadorListadoDivisas.ViewHolder> {

    private final Context context;
    static List<Datos> localDataSet;
    static List<Datos> copyDatos = new ArrayList<Datos>();

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView valorDato;
        private final TextView nombreDato;
        private final Button verDetalles;


        public ViewHolder(View view) {
            super(view);
            // Define click listener for the ViewHolder's View
            valorDato = (TextView) view.findViewById(R.id.ValorDatos);
            verDetalles = (Button) view.findViewById(R.id.btnVerDetalles);
            nombreDato = (TextView) view.findViewById(R.id.NombreDato);

        }

        public Button getVerDetalles() {
            return verDetalles;
        }

        public TextView getNombreDato() {
            return nombreDato;
        }

        public TextView getValorDato() {
            return valorDato;
        }
    }

    /**
     * Initialize the dataset of the Adapter.
     *
     * @param dataSet            String[] containing the data to populate views to be used
     *                           by RecyclerView.
     * @param applicationContext
     */
    public AdaptadorListadoDivisas(List<Datos> dataSet, Context applicationContext) {
        localDataSet = dataSet;
        this.context = applicationContext;
        this.copyDatos.addAll(dataSet);
    }

    // Create new views (invoked by the layout manager)

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view, which defines the UI of the list item
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.cardview, viewGroup, false);

        return new ViewHolder(view);
    }
    // Replace the contents of a view (invoked by the layout manager)

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, @SuppressLint("RecyclerView") final int position) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        viewHolder.getVerDetalles().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DetallesDivisasActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("Objeto", (Serializable) localDataSet.get(position));
                context.startActivity(intent);
            }
        });
        viewHolder.getNombreDato().setText(localDataSet.get(position).getNombre());
        viewHolder.getValorDato().setText(localDataSet.get(position).getValor().toString());
    }
    // Return the size of your dataset (invoked by the layout manager)

    @Override
    public int getItemCount() {
        return localDataSet.size();
    }


    public void filtrar(CharSequence texto) {
        localDataSet.clear();
        if (texto.length() == 0) {
            localDataSet.addAll(copyDatos);
        } else {
            for (Datos datos : copyDatos) {

                String cadenaNormalize = Normalizer.normalize(datos.getNombre().toLowerCase(), Normalizer.Form.NFD);
                String cadenaSinAcentos = cadenaNormalize.replaceAll("[^\\p{ASCII}]", "");

                if (cadenaSinAcentos.contains(texto.toString().toLowerCase())) {
                    localDataSet.add(datos);
                }
            }
        }
        notifyDataSetChanged();
    }

}


