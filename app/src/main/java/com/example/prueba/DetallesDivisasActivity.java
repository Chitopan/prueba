package com.example.prueba;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Toast;

import com.example.prueba.contract.DetallesDivisasContract;
import com.example.prueba.presentadores.DetallesDivisasPresentador;

public class DetallesDivisasActivity extends AppCompatActivity implements DetallesDivisasContract.View {

    MultiAutoCompleteTextView texto;
    DetallesDivisasContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles);

        presenter = new DetallesDivisasPresentador(this);

        texto = findViewById(R.id.multiAutoCompleteTextView);
        presenter = new DetallesDivisasPresentador(this);

        presenter.obtenerDatosSerializables(texto);

    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }

    @Override
    public void onSuccess(String mensaje) {
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(String mensaje) {
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }
}