package com.example.prueba;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.example.prueba.adaptadores.AdaptadorListadoDivisas;
import com.example.prueba.contract.ListadoDivisasContract;
import com.example.prueba.presentadores.ListadoDivisasPresentador;

public class ListadoDivisasActivity extends AppCompatActivity implements ListadoDivisasContract.View {

    private EditText buscarCodigo;
    private RecyclerView mRecyclerView;
    private AdaptadorListadoDivisas mAdapter;

    ListadoDivisasPresentador listadoDivisasPresentador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(layoutManager);

        listadoDivisasPresentador = new ListadoDivisasPresentador(this, mRecyclerView);
        listadoDivisasPresentador.solicitarDatosDivisas();

        buscarCodigo = findViewById(R.id.textInputBuscarCodigo);
        buscarCodigo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mAdapter = listadoDivisasPresentador.getmAdapter();
                mAdapter.filtrar(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menuopciones, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.opcion1) {
            listadoDivisasPresentador.cerrarSesion();
            return true;
        }
        return false;
    }


    @Override
    public void onSuccess(String mensaje) {
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(String mensaje) {
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }
}