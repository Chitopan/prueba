package com.example.prueba;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.prueba.contract.LoginActivityContract;
import com.example.prueba.presentadores.LoginActivityPresentador;

public class LoginActivity extends AppCompatActivity implements LoginActivityContract.View {

    private EditText inputTextUsuario, inputTextContraseña;
    private LoginActivityContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button btnlogin = findViewById(R.id.buttonLogin);
        inputTextUsuario = findViewById(R.id.inputLoginUsuario);
        inputTextContraseña = findViewById(R.id.inputLoginContraseña);

        presenter = new LoginActivityPresentador(this, LoginActivity.this,getApplicationContext());

        presenter.iniciarSharedPreferences();

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usuario = inputTextUsuario.getText().toString();
                String contraseña = inputTextContraseña.getText().toString();
                presenter.iniciarSesion(usuario, contraseña);
            }
        });
    }

    @Override
    public void onSuccess(String mensaje) {
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(String mensaje) {
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }
}