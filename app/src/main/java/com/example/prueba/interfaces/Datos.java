package com.example.prueba.interfaces;

public abstract class Datos {

    public abstract String getNombre();

    public abstract Double getValor();

}
