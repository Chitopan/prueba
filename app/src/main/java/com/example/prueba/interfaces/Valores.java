package com.example.prueba.interfaces;

import com.example.prueba.modelos.Example;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Valores {

    @GET("api/")
    Call<Example> getDivisa();

}
