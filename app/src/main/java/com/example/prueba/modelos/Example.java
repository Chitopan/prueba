package com.example.prueba.modelos;

import javax.annotation.Generated;

import com.example.prueba.interfaces.Valores;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import retrofit2.Call;

@Generated("jsonschema2pojo")
public class Example  {

    @SerializedName("version")
    @Expose
    private String version;
    @SerializedName("autor")
    @Expose
    private String autor;
    @SerializedName("fecha")
    @Expose
    private String fecha;
    @SerializedName("uf")
    @Expose
    private Uf uf;
    @SerializedName("ivp")
    @Expose
    private Ivp ivp;
    @SerializedName("dolar")
    @Expose
    private Dolar dolar;
    @SerializedName("dolar_intercambio")
    @Expose
    private DolarIntercambio dolarIntercambio;
    @SerializedName("euro")
    @Expose
    private Euro euro;
    @SerializedName("ipc")
    @Expose
    private Ipc ipc;
    @SerializedName("utm")
    @Expose
    private Utm utm;
    @SerializedName("imacec")
    @Expose
    private Imacec imacec;
    @SerializedName("tpm")
    @Expose
    private Tpm tpm;
    @SerializedName("libra_cobre")
    @Expose
    private LibraCobre libraCobre;
    @SerializedName("tasa_desempleo")
    @Expose
    private TasaDesempleo tasaDesempleo;
    @SerializedName("bitcoin")
    @Expose
    private Bitcoin bitcoin;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Uf getUf() {
        return uf;
    }

    public void setUf(Uf uf) {
        this.uf = uf;
    }

    public Ivp getIvp() {
        return ivp;
    }

    public void setIvp(Ivp ivp) {
        this.ivp = ivp;
    }

    public Dolar getDolar() {
        return dolar;
    }

    public void setDolar(Dolar dolar) {
        this.dolar = dolar;
    }

    public DolarIntercambio getDolarIntercambio() {
        return dolarIntercambio;
    }

    public void setDolarIntercambio(DolarIntercambio dolarIntercambio) {
        this.dolarIntercambio = dolarIntercambio;
    }

    public Euro getEuro() {
        return euro;
    }

    public void setEuro(Euro euro) {
        this.euro = euro;
    }

    public Ipc getIpc() {
        return ipc;
    }

    public void setIpc(Ipc ipc) {
        this.ipc = ipc;
    }

    public Utm getUtm() {
        return utm;
    }

    public void setUtm(Utm utm) {
        this.utm = utm;
    }

    public Imacec getImacec() {
        return imacec;
    }

    public void setImacec(Imacec imacec) {
        this.imacec = imacec;
    }

    public Tpm getTpm() {
        return tpm;
    }

    public void setTpm(Tpm tpm) {
        this.tpm = tpm;
    }

    public LibraCobre getLibraCobre() {
        return libraCobre;
    }

    public void setLibraCobre(LibraCobre libraCobre) {
        this.libraCobre = libraCobre;
    }

    public TasaDesempleo getTasaDesempleo() {
        return tasaDesempleo;
    }

    public void setTasaDesempleo(TasaDesempleo tasaDesempleo) {
        this.tasaDesempleo = tasaDesempleo;
    }

    public Bitcoin getBitcoin() {
        return bitcoin;
    }

    public void setBitcoin(Bitcoin bitcoin) {
        this.bitcoin = bitcoin;
    }

    @Override
    public String toString() {
        return "Example{" +
                "version='" + version + '\'' +
                ", autor='" + autor + '\'' +
                ", fecha='" + fecha + '\'' +
                ", uf=" + uf +
                ", ivp=" + ivp +
                ", dolar=" + dolar +
                ", dolarIntercambio=" + dolarIntercambio +
                ", euro=" + euro +
                ", ipc=" + ipc +
                ", utm=" + utm +
                ", imacec=" + imacec +
                ", tpm=" + tpm +
                ", libraCobre=" + libraCobre +
                ", tasaDesempleo=" + tasaDesempleo +
                ", bitcoin=" + bitcoin +
                '}';
    }


}